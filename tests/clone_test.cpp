#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_version_macros.hpp>
#include <catch2/trompeloeil.hpp>

#include "mock/mock_appender.hpp"

#include <sl/appenders.hpp>
#include <sl/sl.hpp>
#include <sl/transformers.hpp>

#include <memory>

TEST_CASE("Test if it's possible to clone the logger", "[clone]") {
    auto a = std::make_unique<MockAppender>();
    const std::string parentPrefix = "logger";
    const std::string childPrefix = "child";

    sl::Logger l{parentPrefix};
    auto childLogger = l.clone(childPrefix);

    REQUIRE(childLogger != nullptr);
}

TEST_CASE("Test if prefix is correct", "[clone]") {
    auto a = std::make_unique<MockAppender>();
    std::string capturedOutput;

    const std::string parentPrefix = "logger";
    const std::string childPrefix = "child";

    REQUIRE_CALL(*a, log(SL_INFO, ANY(std::string)))
        .LR_SIDE_EFFECT(capturedOutput = _2)
        .TIMES(1, 1);

    // both child and parent will flush the appender on destruction
    REQUIRE_CALL(*a, flush()).TIMES(2, 2);

    sl::Logger l{parentPrefix};
    l.addAppender(std::move(a));
    auto childLogger = l.clone(childPrefix);

    childLogger->setFormat("{prefix}");
    childLogger->log(SL_INFO, "not important");

    const auto expected = parentPrefix + "." + childPrefix;
    REQUIRE(capturedOutput == expected);
}

TEST_CASE("Test if message format is maintained in parent", "[clone]") {
    auto a = std::make_unique<MockAppender>();
    std::string capturedOutput;

    const std::string parentPrefix = "logger";
    const std::string childPrefix = "child";

    REQUIRE_CALL(*a, log(SL_INFO, ANY(std::string)))
        .LR_SIDE_EFFECT(capturedOutput = _2)
        .TIMES(2, 2);

    // both child and parent will flush the appender on destruction
    REQUIRE_CALL(*a, flush()).TIMES(2, 2);

    sl::Logger l{parentPrefix};
    l.addAppender(std::move(a));
    auto childLogger = l.clone(childPrefix);

    l.setFormat("{prefix} {level} {msg}");
    childLogger->setFormat("{prefix}");

    childLogger->log(SL_INFO, "not important");
    const auto expectedChild = parentPrefix + "." + childPrefix;
    REQUIRE(capturedOutput == expectedChild);

    l.log(SL_INFO, "this is important");
    const auto expectedParent =
        parentPrefix + " " + "INFO" + " this is important";
    REQUIRE(capturedOutput == expectedParent);
}

TEST_CASE("Test if loglevels are applied independently", "[clone]") {
    auto a = std::make_unique<MockAppender>();
    std::string capturedOutput;

    const std::string parentPrefix = "logger";
    const std::string childPrefix = "child";

    REQUIRE_CALL(*a, log(ANY(sl::Logger::Level), ANY(std::string)))
        .LR_SIDE_EFFECT(capturedOutput = _2)
        .TIMES(2, 2);

    // both child and parent will flush the appender on destruction
    REQUIRE_CALL(*a, flush()).TIMES(2, 2);

    sl::Logger l{parentPrefix};
    l.setFormat("{msg}");

    l.addAppender(std::move(a));
    auto childLogger = l.clone(childPrefix);

    // Only ERROR level logs should be written out by both parent and child
    l.setLevel(SL_ERROR);

    childLogger->log(SL_INFO, "not important");
    std::string expectedChild = "";
    REQUIRE(capturedOutput == expectedChild);

    l.log(SL_INFO, "neither important");
    std::string expectedParent = "";
    REQUIRE(capturedOutput == expectedParent);

    childLogger->setLevel(SL_DEBUG);
    childLogger->log(SL_INFO, "this should still be silenced out");
    expectedChild = "";
    REQUIRE(capturedOutput == expectedChild);

    const auto msg = "this should be finally logged";
    childLogger->log(SL_ERROR, msg);
    expectedChild = msg;
    REQUIRE(capturedOutput == expectedChild);

    l.setLevel(SL_INFO);
    const auto msg2 = "this should be logged as well";
    childLogger->log(SL_INFO, msg2);
    expectedChild = msg2;
    REQUIRE(capturedOutput == expectedChild);
}

TEST_CASE("Test if format can be applied independently", "[clone]") {
    auto a = std::make_unique<MockAppender>();
    std::string capturedOutput;

    const std::string parentPrefix = "logger";
    const std::string childPrefix = "child";

    REQUIRE_CALL(*a, log(ANY(sl::Logger::Level), ANY(std::string)))
        .LR_SIDE_EFFECT(capturedOutput = _2)
        .TIMES(2, 2);

    // both child and parent will flush the appender on destruction
    REQUIRE_CALL(*a, flush()).TIMES(2, 2);

    // configure parent
    sl::Logger l{parentPrefix};
    l.setFormat("{msg}");
    l.setLevel(SL_INFO);
    l.addAppender(std::move(a));

    // configure child
    auto childLogger = l.clone(childPrefix);
    childLogger->setFormat("{level} {prefix} {msg}");

    // log with parent
    const auto parentMsg = "this is logged by the parent";
    l.log(SL_INFO, parentMsg);

    // verify output produced by parent
    std::stringstream ls;
    ls << SL_INFO;
    const std::string levelStr = ls.str();
    const std::string expectedParent = parentMsg;
    REQUIRE(capturedOutput == expectedParent);

    // log with child
    const auto childMsg = "this is logged by the child";
    childLogger->log(SL_INFO, childMsg);

    // verify output produced by child
    const std::string expectedChild =
        levelStr + " " + parentPrefix + "." + childPrefix + " " + childMsg;
    REQUIRE(capturedOutput == expectedChild);
}
