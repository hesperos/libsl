#include <benchmark/benchmark.h>

#include <sl/appender.hpp>
#include <sl/sl.hpp>

#include <chrono>
#include <thread>

class FakeAppender : public sl::Appender {
public:
    void log(sl::Logger::Level, const std::string&) override {
        // simulate IO operation
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    void flush() override {
    }
};

static void BM_Logging(benchmark::State& state) {
    sl::Logger l{"logger"};
    l.addAppender(std::make_unique<FakeAppender>());
    l.addAppender(std::make_unique<FakeAppender>());
    l.addAppender(std::make_unique<FakeAppender>());

    for (auto _ : state) {
        l.log(SL_INFO, "this is a log entry");
    }
}
BENCHMARK(BM_Logging);

BENCHMARK_MAIN();
