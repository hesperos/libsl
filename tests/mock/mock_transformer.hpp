#ifndef _MOCK_TRANSFORMER_HPP_
#define _MOCK_TRANSFORMER_HPP_

#include <sl/transformer.hpp>

class MockTransformer : public sl::Transformer {
public:
    MAKE_MOCK1(transform, std::string(std::string));
};

#endif // !_MOCK_TRANSFORMER_HPP_
