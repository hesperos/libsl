#ifndef _MOCK_APPENDER_HPP_
#define _MOCK_APPENDER_HPP_

#include <sl/appender.hpp>

class MockAppender : public sl::Appender {
public:
    MAKE_MOCK0(flush, void());
    MAKE_MOCK2(log, void(sl::Logger::Level, const std::string&));
};

#endif // !_MOCK_APPENDER_HPP_
