#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_version_macros.hpp>
#include <catch2/trompeloeil.hpp>

#include "mock/mock_appender.hpp"
#include "mock/mock_transformer.hpp"

#include <sl/appenders.hpp>
#include <sl/sl.hpp>
#include <sl/transformers.hpp>

#include <memory>
#include <sstream>
#include <unordered_map>

TEST_CASE("Test if it's possible to set the format", "[logger]") {
    sl::Logger l{"prefix"};
    l.setFormat("{timestamp} {level} {prefix} -> {msg}");
}

TEST_CASE("Test log output", "[logger]") {
    sl::Logger l{"prefix"};

    std::stringstream ss;
    auto app = std::make_unique<sl::appender::Ostream>(ss);

    l.addAppender(std::move(app));
    l.setFormat("{level} {prefix} -> {msg}");

    l.log(sl::Logger::Level::Info, "test message");

    const auto expected = "INFO prefix -> test message\n";
    REQUIRE(ss.str() == expected);
}

TEST_CASE("Test if there's no log output when logging level is too high",
          "[logger]") {
    sl::Logger l{"some prefix"};

    std::stringstream ss;
    auto app = std::make_unique<sl::appender::Ostream>(ss);

    l.addAppender(std::move(app));
    l.setLevel(sl::Logger::Level::Error);

    l.log(sl::Logger::Level::Info, "test message");
    REQUIRE(ss.str().size() == 0);
}

TEST_CASE("Test if lowercase transformer is translating the message",
          "[transformers]") {
    auto t = sl::transformer::Lowercase{};

    const std::unordered_map<std::string, std::string> testData{
        {"This is a Mixed Case STRING", "this is a mixed case string"},
        {"ALL CAPS STRING", "all caps string"},
        {"all lowercase string", "all lowercase string"},
    };

    for (const auto& dataRow : testData) {
        REQUIRE(dataRow.second == t.transform(dataRow.first));
    }
}

TEST_CASE("Test if NoTrailingBlank transformer is trimming strings",
          "[transformers]") {
    auto t = sl::transformer::NoTrailingBlank{};

    const std::unordered_map<std::string, std::string> testData{
        {"This has newline\n", "This has newline"},
        {"This has newlines\n\n\n", "This has newlines"},
        {"This has newlines and spaces \n\n \t ",
         "This has newlines and spaces"},
    };

    for (const auto& dataRow : testData) {
        REQUIRE(dataRow.second == t.transform(dataRow.first));
    }
}

TEST_CASE("Test if OstreamAppender drops output on level too low",
          "[appenders]") {
    std::stringstream ss;
    auto a = sl::appender::Ostream{ss, SL_WARNING};

    a.log(SL_INFO, "this is a test message");
    REQUIRE(ss.str().size() == 0);

    const std::string shouldBeLogged = "this message should be logged";
    a.log(SL_WARNING, shouldBeLogged);

    // include "new line character"
    REQUIRE(ss.str().size() == shouldBeLogged.size() + 1);
}

TEST_CASE("Test if logger sends output to all appenders", "[logger]") {
    auto a1 = std::make_unique<MockAppender>();
    auto a2 = std::make_unique<MockAppender>();

    REQUIRE_CALL(*a1, log(SL_INFO, ANY(std::string))).TIMES(1, 1);

    REQUIRE_CALL(*a2, log(SL_INFO, ANY(std::string))).TIMES(1, 1);

    REQUIRE_CALL(*a1, flush()).TIMES(1, 1);

    REQUIRE_CALL(*a2, flush()).TIMES(1, 1);

    sl::Logger l{"some prefix"};
    l.setLevel(SL_DEBUG);

    l.addAppender(std::move(a1));
    l.addAppender(std::move(a2));

    l.log(SL_INFO, "this is a very important debug");
}

TEST_CASE("Test if logger is silent if loglevel too low", "[logger]") {
    using trompeloeil::_;
    auto a = std::make_unique<MockAppender>();

    FORBID_CALL(*a, log(ANY(sl::Logger::Level), ANY(std::string)));

    REQUIRE_CALL(*a, flush()).TIMES(1, 1);

    {
        sl::Logger l{"some prefix"};
        l.setLevel(SL_WARNING);
        l.addAppender(std::move(a));
        l.log(SL_INFO, "this should never be routed to an appender");
    }
}

TEST_CASE("Test if logger applies transformers", "[logger]") {
    auto a = std::make_unique<MockAppender>();
    auto t = std::make_unique<MockTransformer>();

    const std::string transformedMsg = "this is the transformed msg";

    REQUIRE_CALL(*t, transform(ANY(std::string)))
        .TIMES(1, 1)
        .RETURN(transformedMsg);

    REQUIRE_CALL(*a, log(ANY(sl::Logger::Level), transformedMsg));

    REQUIRE_CALL(*a, flush()).TIMES(1, 1);

    {
        sl::Logger l{"some prefix"};
        l.setLevel(SL_INFO);
        l.setFormat("{msg}");
        l.addAppender(std::move(a));
        l.addTransformer(std::move(t));
        l.log(SL_INFO, "this message should be transformed");
    }
}

TEST_CASE("Test if various format fields produce output", "[logger]") {
    using trompeloeil::_;

    auto a = std::make_unique<MockAppender>();
    std::string capturedOutput;

    REQUIRE_CALL(*a, log(ANY(sl::Logger::Level), ANY(std::string)))
        .LR_SIDE_EFFECT(capturedOutput = _2)
        .TIMES(AT_LEAST(1));

    REQUIRE_CALL(*a, flush()).TIMES(1, 1);

    const std::string prefix = "some prefix";
    const std::string msg = "message to be logged";

    sl::Logger l{prefix};
    l.addAppender(std::move(a));
    l.setLevel(SL_INFO);

    l.setFormat("{msg}");
    l.log(SL_INFO, msg);
    REQUIRE(capturedOutput == msg);

    l.setFormat("{prefix}");
    l.log(SL_INFO, msg);
    REQUIRE(capturedOutput == prefix);

    l.setFormat("{level}");
    l.log(SL_INFO, msg);
    std::stringstream ls;
    ls << SL_INFO;
    const std::string levelStr = ls.str();
    REQUIRE(capturedOutput == levelStr);

    l.setFormat("{timestamp}");
    l.log(SL_INFO, msg);
    REQUIRE(capturedOutput.size() > 0);

    l.setFormat("{source_filename}");
    l.log(SL_INFO, msg);
    REQUIRE(capturedOutput.size() > 0);

    l.setFormat("{source_function}");
    l.log(SL_INFO, msg);
    REQUIRE(capturedOutput.size() > 0);

    l.setFormat("{source_line}");
    l.log(SL_INFO, msg);
    REQUIRE(capturedOutput.size() > 0);

    l.setFormat("{source_column}");
    l.log(SL_INFO, msg);
    REQUIRE(capturedOutput.size() > 0);
}

TEST_CASE("Test close drops appenders", "[logger]") {
    auto a = std::make_unique<MockAppender>();

    REQUIRE_CALL(*a, log(ANY(sl::Logger::Level), ANY(std::string))).TIMES(1, 1);

    REQUIRE_CALL(*a, flush()).TIMES(1, 1);

    const std::string prefix = "some prefix";
    const std::string msg = "message to be logged";

    sl::Logger l{prefix};
    l.addAppender(std::move(a));
    l.setLevel(SL_INFO);

    l.log(SL_INFO, "this should be logged");

    l.close();

    l.log(SL_INFO, "logger should've dropped appenders.  No further output "
                   "should be produced");
}
