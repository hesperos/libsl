project('sl', 'cpp',
  version : '0.0.10',
  default_options : ['warning_level=3', 'cpp_std=c++2a'])

cxx = meson.get_compiler('cpp')
libtbb = cxx.find_library('tbb', required : false)

fmt_proj = subproject('fmt')
fmt_dep = fmt_proj.get_variable('fmt_dep')

thread_dep = dependency('threads')

inc_dir = include_directories('include')

libsl_cpp_args = [ ]
libsl_deps = [ fmt_dep, thread_dep ]

if get_option('with_tbb') and libtbb.found()
  libsl_deps += [ libtbb ]
  libsl_cpp_args += [ '-DWITH_TBB' ]
endif

libsl_lib = library('sl',
           'src/appenders.cpp',
           'src/factory.cpp',
           'src/logger.cpp',
           'src/scoped_log.cpp',
           'src/transformers.cpp',
           cpp_args : libsl_cpp_args,
           include_directories : [ inc_dir ],
           dependencies : libsl_deps,
           version : meson.project_version(),
           install : true)

libsl_dep = declare_dependency(link_with : [ libsl_lib ],
  dependencies : libsl_deps,
  include_directories : [ inc_dir ])

if get_option('with_examples')
  subdir('examples')
endif

if get_option('buildtype') != 'release'
  subdir('tests')
endif
