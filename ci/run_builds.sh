#!/bin/bash

meson setup asan_bld
meson configure -Db_sanitize=address asan_bld

meson setup tsan_bld
meson configure -Db_sanitize=thread tsan_bld

# run both in parallel
meson compile -v -C asan_bld &
meson compile -v -C tsan_bld &

# wait for all to complete
wait
