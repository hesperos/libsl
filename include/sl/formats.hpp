#ifndef _FORMATS_HPP_
#define _FORMATS_HPP_

namespace sl::format {

inline constexpr const char* Verbose =
    "{sn} {timestamp} {level} {prefix} {thread_id} "
    "{source_filename}:{source_function}:{source_line} -> {msg}";

inline constexpr const char* WithSourceInfo =
    "{timestamp} {level} {prefix} "
    "{source_filename}:{source_function}:{source_line} -> {msg}";

inline constexpr const char* Default = "{timestamp} {level} {prefix} -> {msg}";

inline constexpr const char* Minimal = "{level} {prefix} -> {msg}";

inline constexpr const char* PlainMsg = "{msg}";
} // namespace sl::format

#endif // !_FORMATS_HPP_
