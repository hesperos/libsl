#ifndef _TRANSFORMER_HPP_
#define _TRANSFORMER_HPP_

#include <string>

namespace sl {
/**
 * @class Transformer
 * @brief Transforms the log message
 *
 */
class Transformer {
public:
    virtual ~Transformer() = default;

    /**
     * @brief Performs transformation
     *
     * This can be use to remove vulnerable data from the log contents or to
     * perform log message transformations.
     *
     * @param msg message is provided by the logger
     */
    virtual std::string transform(std::string msg) = 0;
};
} // namespace sl

#endif // !_TRANSFORMER_HPP_
