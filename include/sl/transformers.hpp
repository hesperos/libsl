#ifndef _TRANSFORMERS_HPP_
#define _TRANSFORMERS_HPP_

#include <sl/transformer.hpp>

namespace sl::transformer {

class Lowercase : public sl::Transformer {
public:
    std::string transform(std::string msg) override;
};

class NoTrailingBlank : public sl::Transformer {
public:
    std::string transform(std::string msg) override;
};
} // namespace sl::transformer

#endif // !_TRANSFORMERS_HPP_
