#ifndef _SL_HPP_
#define _SL_HPP_

#include <sl/appender.hpp>
#include <sl/factory.hpp>
#include <sl/formats.hpp>
#include <sl/logger.hpp>
#include <sl/scoped_log.hpp>

// include fmt by default
#include <fmt/core.h>

#endif // !_SL_HPP_
