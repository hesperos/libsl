#ifndef _SCOPED_LOG_HPP_
#define _SCOPED_LOG_HPP_

#include <sl/logger.hpp>

namespace sl {

class ScopedLog {
public:
    ScopedLog(sl::Logger& logger, sl::Logger::Level level = SL_INFO,
              std::source_location sl = std::source_location::current());

    ScopedLog(const ScopedLog&) = delete;
    ScopedLog& operator=(const ScopedLog&) = delete;

    ~ScopedLog();

private:
    sl::Logger& logger;
    const sl::Logger::Level level;
    const std::source_location sl;
};

} // namespace sl

#endif // !_SCOPED_LOG_HPP_
