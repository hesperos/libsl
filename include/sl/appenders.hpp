#ifndef _OSTREAM_APPENDER_HPP_
#define _OSTREAM_APPENDER_HPP_

#include <sl/appender.hpp>

#include <memory>
#include <mutex>
#include <ostream>

namespace sl::appender {

/**
 * @class Ostream
 * @brief Writes the logs to provided std::ostream output
 *
 * Works with std::cout, std::err, std::stringstream and std::fstreams
 *
 */
class Ostream : public Appender {
public:
    /**
     * @brief Constructs the appender
     *
     * @param os the output stream to use for writing
     * @param minLevel minimum logging level.  If the level provided to `log`
     * call is below this level, the log message will be dropped.
     */
    Ostream(std::ostream& os, Logger::Level minLevel = Logger::Level::lowest);

    /**
     * @brief Writes log entry
     *
     * @param l Log level of the entry
     * @param msg log message
     */
    void log(Logger::Level l, const std::string& msg) override;

    /**
     * @brief Flushes the underlying output stream
     */
    void flush() override;

private:
    std::reference_wrapper<std::ostream> os;
    Logger::Level minLevel;
};

/**
 * @class Locked
 * @brief Wrapper for appenders executing with mutex locked
 *
 */
class Locked : public Appender {
public:
    /**
     * @brief Constructs an appender
     * @param app Appender to be wrapped
     */
    Locked(std::unique_ptr<Appender> app);

    /**
     * @brief Calls `log` of the wrapped appender with lock held
     *
     * @param l
     * @param msg
     */
    void log(Logger::Level l, const std::string& msg) override;

    /**
     * @brief Calls `flush` of the wrapped appender with lock held
     */
    void flush() override;

private:
    std::mutex mtx;
    std::unique_ptr<Appender> app;
};

} // namespace sl::appender

#endif // !_OSTREAM_APPENDER_HPP_
