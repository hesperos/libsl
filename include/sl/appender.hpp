#ifndef _APPENDER_HPP_
#define _APPENDER_HPP_

#include <sl/logger.hpp>

#include <string>

namespace sl {

/**
 * @class Appender
 * @brief Appender is an interface for output formatted log messages
 *
 * Logger can have multiple appenders, it first constructs a complete message
 * line and then pass it to all registered appenders.  Appenders render output.
 *
 * Inherit from this class and implement its interface to introduce new
 * appenders.
 *
 */
class Appender {
public:
    virtual ~Appender() = default;

    /**
     * @brief log is called by logger to record a log entry
     *
     * @param l severity of the log entry
     * @param msg log entry to be logged
     *
     * Loglevel can be used by appender implementations to route the message
     * accordingly or to drop the message alltogether.
     */
    virtual void log(Logger::Level l, const std::string& msg) = 0;

    /**
     * @brief Flushes the output
     */
    virtual void flush() = 0;
};

} // namespace sl

#endif // !_APPENDER_HPP_
