#ifndef _FACTORY_HPP_
#define _FACTORY_HPP_

#include <sl/logger.hpp>

namespace sl {

/**
 * @brief Constructs a logger writing to std::cout and std::cerr
 *
 * All log messages will be logged to std::cout.  Additionally, all messages
 * with level `SL_WARNING` and above will be logged to std::cerr.
 *
 * The default level is SL_INFO.
 * The format is sl::format::Default.
 *
 * @param prefix prefix to use for each log message
 * @return
 */
Logger makeStreamsLogger(std::string prefix);

} // namespace sl

#endif // !_FACTORY_HPP_
