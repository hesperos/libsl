#ifndef _LOGGER_HPP_
#define _LOGGER_HPP_

#include <memory>
#include <source_location>

#include <fmt/core.h>

#define SL_TRACE sl::Logger::Level::Trace
#define SL_DEBUG sl::Logger::Level::Debug
#define SL_INFO sl::Logger::Level::Info
#define SL_WARNING sl::Logger::Level::Warning
#define SL_ERROR sl::Logger::Level::Error

namespace sl {

class Appender;
class Transformer;

/**
 * @class Logger
 * @brief Implementation of a simple logger
 *
 */
class Logger {
public:
    enum Level {
        lowest,

        Trace,
        Debug,
        Info,
        Warning,
        Error,

        highest,
    };

private:
    struct LevelWrapper {
        const Level level;
        const std::source_location sourceLocation;

        /**
         * @brief Captures source_location by performing implicit conversion
         * from Logger::Level
         *
         * @param level
         * @param sl
         */
        LevelWrapper(Level level,
                     std::source_location sl = std::source_location::current());
    };

public:
    ~Logger();

    /**
     * @brief Constructs a logger
     *
     * @param prefix prefix for every log entry (can be used to identify the
     * origin)
     */
    explicit Logger(std::string prefix);

    Logger(Logger&&);

    Logger(const Logger&) = delete;
    Logger& operator=(const Logger&) = delete;

    std::shared_ptr<Logger> clone(std::string prefix);

    /**
     * @brief Allows for customisation of the log message
     */
    void setFormat(std::string);

    /**
     * @brief Allows to set the minimum level of the messages that will be
     * logged
     *
     * @param l
     */
    void setLevel(Level l);

    /**
     * @brief Adds a message transformer to the logger
     *
     * Transformers are applied to log messages to process the output.
     *
     * @param t an instance of the transformer
     */
    void addTransformer(std::unique_ptr<Transformer> t);

    /**
     * @brief Adds an appender to the logger
     *
     * Appenders are responsible for writing the log messages to output.  Logger
     * output the same constructed message to all its appenders.
     *
     * @param ap an instance of the appender
     */
    void addAppender(std::unique_ptr<Appender> ap);

    /**
     * @brief Logs a message
     *
     * @param l log level of the message (if the global level configured on the
     * logger is higher than this, the message won't be logged)
     * @param msg message to be logged
     * @param sl source location - this should rarely be ever overriden
     */
    void log(const Level& l, std::string msg,
             std::source_location sl = std::source_location::current());

    template <typename... ArgsT>
    constexpr void logfmt(LevelWrapper lw, std::string_view fmtStr,
                          ArgsT&&... args) {
        log(lw.level,
            fmt::format(fmt::runtime(fmtStr), std::forward<ArgsT>(args)...),
            lw.sourceLocation);
    }

    /**
     * @brief Closes all logger's appenders.  Logger usage is permitted after
     * this call - but no output will ever be produced as all appenders have
     * been flushed and destroyed.
     */
    void close();

private:
    class Impl;
    std::shared_ptr<Impl> pImpl;
};

std::ostream& operator<<(std::ostream& os, const sl::Logger::Level& l);

} // namespace sl

#endif // !_LOGGER_HPP_
