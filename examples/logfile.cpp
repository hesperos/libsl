#include <sl/appenders.hpp>
#include <sl/sl.hpp>

#include <fstream>

namespace {
auto logger = sl::makeStreamsLogger("appender example");
}

void count(int i) {
    while (--i >= 0) {
        logger.log(SL_INFO, fmt::format("{} bottles of beer", i));
    }
    logger.log(SL_INFO, "done");
}

int main(int, char*[]) {
    std::ofstream ofs{"logfile.log"};

    // wrapped with appender::Locked to make it thread safe
    auto a = std::make_unique<sl::appender::Locked>(
        std::make_unique<sl::appender::Ostream>(ofs));

    logger.addAppender(std::move(a));

    count(100);

    // logger outlives the ofs so, close it explicitly
    logger.close();

    return 0;
}
