#include <sl/sl.hpp>

namespace {
auto logger = sl::makeStreamsLogger("mylog");
}

int divide(int a, int b) {
    if (b == 0) {
        logger.log(SL_ERROR, "can't divide by zero!");
        return 0;
    }

    logger.log(SL_TRACE, fmt::format("dividing a: {} by b: {}", a, b));
    return a / b;
}

int main(int argc, char* argv[]) {
    logger.log(SL_INFO, "welcome to the division program");
    logger.setFormat(sl::format::WithSourceInfo);

    if (argc > 1 && std::string(argv[1]) == "--verbose") {
        logger.setLevel(SL_TRACE);
    }

    logger.logfmt(SL_INFO, "about to divide {} by {}", 10, 5);

    divide(10, 5);
    divide(2, 0);
    return 0;
}
