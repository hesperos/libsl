#include <sl/appenders.hpp>
#include <sl/sl.hpp>

#include <iostream>
#include <sstream>
#include <thread>

namespace {
std::stringstream ss;
auto logger = sl::Logger{"appender example"};
} // namespace

void count(int i) {
    while (--i >= 0) {
        logger.log(SL_INFO, fmt::format("{} bottles of beer", i));
    }
    logger.log(SL_INFO, "done");
}

int main(int, char*[]) {
    // wrapped with appender::Locked to make it thread safe
    auto a = std::make_unique<sl::appender::Locked>(
        std::make_unique<sl::appender::Ostream>(ss));

    logger.addAppender(std::move(a));
    logger.setFormat("{thread_id} {level} {msg}");

    std::jthread t(count, 100);
    count(100);
    t.join();

    fmt::print("{}", ss.str());
    return 0;
}
