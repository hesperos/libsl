#include <sl/sl.hpp>

namespace {
auto parentLogger = sl::makeStreamsLogger("mylog");
}

class X {
public:
    X() :
        logger{parentLogger.clone("X")} {
        logger->log(SL_TRACE, "ctor");
    }

    ~X() {
        logger->log(SL_TRACE, "dtor");
    }

    void foo() {
        logger->log(SL_INFO, "foo-ing");
    }

private:
    std::shared_ptr<sl::Logger> logger;
};

class Y {
public:
    Y() :
        logger{parentLogger.clone("Y")} {
        logger->log(SL_TRACE, "ctor");
    }

    ~Y() {
        logger->log(SL_TRACE, "dtor");
    }

    void bar() {
        logger->log(SL_INFO, "barring");
    }

private:
    std::shared_ptr<sl::Logger> logger;
};

class Z {
public:
    Z() :
        logger{parentLogger.clone("Z")} {
        // Child logger level can be set additionally to the parent's level.
        // The below will silence Z's traces.
        logger->setLevel(SL_INFO);
        logger->log(SL_TRACE, "ctor");
    }

    ~Z() {
        logger->log(SL_TRACE, "dtor");
    }

    void baz() {
        logger->log(SL_INFO, "buzzing");
    }

private:
    std::shared_ptr<sl::Logger> logger;
};

int main(int, char*[]) {
    parentLogger.setFormat(sl::format::WithSourceInfo);
    parentLogger.setLevel(SL_TRACE);

    parentLogger.log(SL_INFO, "welcome to child loggers demo program");

    X x;
    Y y;
    Z z;

    x.foo();

    // Parent logger levels apply to all output produced by children.
    // The below will silence any output below SL_INFO even from children
    parentLogger.setLevel(SL_INFO);

    y.bar();

    // restoring most verbose level
    parentLogger.setLevel(SL_TRACE);

    z.baz();

    return 0;
}
