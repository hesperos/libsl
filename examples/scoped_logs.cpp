#include <sl/sl.hpp>

#include <chrono>
#include <thread>

namespace {
auto logger = sl::makeStreamsLogger("mylog");
}

int bar() {
    sl::ScopedLog l(logger);
    return 1;
}

int foo() {
    sl::ScopedLog l(logger);
    return bar();
}

int main(int, char*[]) {
    logger.setFormat(sl::format::WithSourceInfo);

    {
        sl::ScopedLog l(logger);
        foo();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    return 0;
}
