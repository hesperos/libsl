[![pipeline status](https://gitlab.com/hesperos/libsl/badges/master/pipeline.svg)](https://gitlab.com/hesperos/libsl/-/commits/master)
[![Latest Release](https://gitlab.com/hesperos/libsl/-/badges/release.svg)](https://gitlab.com/hesperos/libsl/-/releases)

# libsl is a simple formatting library for C++

I've created `libsl` for my personal projects.  It's versatile, lightweight and
easy to use.

## Quick start

1. Declare a logger instance (can be a global variable for convenience
   purposes).
2. That's it - you're ready to go!

Here's a short example
```C++

namespace {
    auto logger = sl::makeStreamsLogger("mylogger");
} // namespace

void foo() {
    logger.log(SL_INFO, "Important stuff happening in foo");
    ...
}

int main (int argc, const char *argv[])
{
    ...
    if (isVerbose) {
        logger.setLevel(sl::Logger::Level::Debug);
    }

    logger.log(SL_DEBUG,
        fmt::format("variables x: {}, y: {}, z: {}", x, y, z~));

    ...
}
```

You can have as many loggers as you want!  `sl::makeStreamsLogger` produces a
logger which outputs to `std::cout` and `std::cerr` streams.  Access to the
streams is protected with a `mutex` so, the logger **can be used** in
multi-threaded environment **safely**.


Checkout [examples](examples) for more details on usage.


## Appenders

Logger can have many appenders.  An appender is simply an output writer
for a log.  The default logger created using `sl::makeStreamsLogger`
writes output to `std::cout` and additionally, repeats `SL_WARNING` and
`SL_ERROR` logs to `std::cerr`.

You can write your own appenders and add them to the logger.  To do
that, implement the interface declared in (appender.hpp)[include/sl/appender.hpp).

## Formatting

The framework provides the following formatting fields:

- `{timestamp}`
- `{level}`
- `{prefix}`
- `{source_filename}`
- `{source_function}`
- `{source_line}`
- `{source_column}`
- `{msg}`
- `{sn}`
- `{thread_id}`

These can be composed any way you like to form the log entry.  `libsl` comes
with a set of predefined log formats:

- `Default`
- `WithSourceInfo`
- `Minimal`
- `PlainMsg`

These can be inspected in the [formats.hpp](include/sl/formats.hpp) file.
