#include <sl/appender.hpp>
#include <sl/formats.hpp>
#include <sl/logger.hpp>
#include <sl/transformer.hpp>

#include <fmt/chrono.h>
#include <fmt/core.h>
#include <fmt/ostream.h>
#include <fmt/std.h>

#include <algorithm>
#include <chrono>
#include <execution>
#include <filesystem>
#include <mutex>
#include <thread>
#include <unordered_map>
#include <vector>

namespace {
constexpr auto defaultFormat = sl::format::Default;
constexpr auto defaultLevel = sl::Logger::Level::Info;
} // namespace

template <> struct fmt::formatter<sl::Logger::Level> : ostream_formatter {};

namespace sl {

std::ostream& operator<<(std::ostream& os, const sl::Logger::Level& l) {
    const std::unordered_map<sl::Logger::Level, std::string> m{
        {sl::Logger::Level::Trace, "TRACE"},
        {sl::Logger::Level::Debug, "DEBUG"},
        {sl::Logger::Level::Info, "INFO"},
        {sl::Logger::Level::Warning, "WARN"},
        {sl::Logger::Level::Error, "ERROR"},
    };

    if (auto it = m.find(l); it != m.end()) {
        os << it->second;
    } else {
        os << "UNKNOWN";
    }

    return os;
}

class LoggerImpl {
public:
    LoggerImpl(std::string prefix) :
        prefix{prefix},
        logFormat{defaultFormat},
        logLevel{defaultLevel},
        sn{0} {
    }

    virtual ~LoggerImpl() {
        flush();
    }

    std::string getPrefix() const {
        return prefix;
    }

    void setFormat(std::string fmt) {
        logFormat = fmt;
    }

    std::string getFormat() const {
        return logFormat;
    }

    void setLevel(Logger::Level l) {
        logLevel = l;
    }

    Logger::Level getLevel() const {
        return logLevel;
    }

    void addAppender(std::unique_ptr<Appender> ap) {
        std::scoped_lock l{appenderMtx};
        appenders.push_back(std::move(ap));
    }

    void addTransformer(std::unique_ptr<Transformer> t) {
        std::scoped_lock l{transformerMtx};
        transformers.push_back(std::move(t));
    }

    void logFormatted(const Logger::Level& l, std::string msg,
                      std::source_location sl) {
        if (l < logLevel) {
            return;
        }

        msg = transformMsg(msg);
        msg = formatMsg(l, msg, sl);
        fanOutMsg(l, msg);
    }

    void logUnformatted(const Logger::Level& l, std::string msg) {
        if (l < logLevel) {
            return;
        }

        msg = transformMsg(msg);
        fanOutMsg(l, msg);
    }

    void flush() {
        std::scoped_lock lck{appenderMtx};
        for (auto& a : appenders) {
            a->flush();
        }
    }

    void close() {
        flush();
        std::scoped_lock lck{appenderMtx};
        appenders.clear();
    }

private:
    const std::string prefix;
    std::string logFormat;
    Logger::Level logLevel;
    std::atomic<uint64_t> sn;

    std::mutex appenderMtx;
    std::vector<std::unique_ptr<Appender>> appenders;

    std::mutex transformerMtx;
    std::vector<std::unique_ptr<Transformer>> transformers;

    std::string transformMsg(std::string msg) {
        std::scoped_lock lck{transformerMtx};
        for (const auto& t : transformers) {
            msg = t->transform(std::move(msg));
        }
        return msg;
    }

    void fanOutMsg(const Logger::Level& l, const std::string& logmsg) {
        std::scoped_lock lck{appenderMtx};
#ifdef WITH_TBB
        const auto execPolicy = std::execution::par;
#else
        const auto execPolicy = std::execution::seq;
#endif
        std::for_each(execPolicy, appenders.begin(), appenders.end(),
                      [&](auto& a) { a->log(l, logmsg); });
    }

    std::string formatMsg(const Logger::Level& l, std::string msg,
                          std::source_location sl) {

        const auto now = std::chrono::system_clock::now();
        const auto fn = std::filesystem::path(sl.file_name()).filename();
        const auto threadId = std::this_thread::get_id();
        const auto& logmsg =
            fmt::format(fmt::runtime(logFormat), fmt::arg("timestamp", now),
                        fmt::arg("level", l), fmt::arg("prefix", prefix),
                        fmt::arg("msg", msg), fmt::arg("sn", sn++),
                        fmt::arg("thread_id", threadId),
                        fmt::arg("source_filename", fn.native()),
                        fmt::arg("source_function", sl.function_name()),
                        fmt::arg("source_line", sl.line()),
                        fmt::arg("source_column", sl.column()));

        return logmsg;
    }
};

class LoggerAppender : public Appender {
public:
    explicit LoggerAppender(std::shared_ptr<LoggerImpl> pImpl) :
        pImpl{pImpl} {
    }

    void log(Logger::Level l, const std::string& msg) override {
        pImpl->logUnformatted(l, msg);
    }

    void flush() override {
        pImpl->flush();
    }

private:
    std::shared_ptr<LoggerImpl> pImpl;
};

class Logger::Impl : public LoggerImpl {
public:
    using LoggerImpl::LoggerImpl;
};

Logger::~Logger() {
}

Logger::Logger(std::string prefix) :
    pImpl{std::make_shared<Impl>(std::move(prefix))} {
}

std::shared_ptr<Logger> Logger::clone(std::string prefix) {
    const std::string childPrefix = pImpl->getPrefix() + "." + prefix;
    auto childLogger = std::make_shared<Logger>(childPrefix);
    childLogger->addAppender(std::make_unique<LoggerAppender>(pImpl));
    childLogger->setFormat(pImpl->getFormat());
    childLogger->setLevel(Level::lowest);
    return childLogger;
}

Logger::Logger(Logger&& other) :
    pImpl{std::move(other.pImpl)} {
}

void Logger::setFormat(std::string s) {
    pImpl->setFormat(s);
}

void Logger::setLevel(Level l) {
    pImpl->setLevel(l);
}

void Logger::addAppender(std::unique_ptr<Appender> ap) {
    pImpl->addAppender(std::move(ap));
}

void Logger::addTransformer(std::unique_ptr<Transformer> t) {
    pImpl->addTransformer(std::move(t));
}

void Logger::log(const Level& l, std::string msg, std::source_location sl) {
    pImpl->logFormatted(l, msg, sl);
}

void Logger::close() {
    pImpl->close();
}

Logger::LevelWrapper::LevelWrapper(Logger::Level level,
                                   std::source_location sl) :
    level{level},
    sourceLocation{sl} {
}

} // namespace sl
