#include <sl/appenders.hpp>

#include <type_traits>

namespace sl::appender {

Ostream::Ostream(std::ostream& os, Logger::Level minLevel) :
    os{os},
    minLevel{minLevel} {
}

void Ostream::log(Logger::Level l, const std::string& msg) {
    if (l >= minLevel) {
        os.get() << msg << std::endl;
    }
}

void Ostream::flush() {
    os.get().flush();
}

Locked::Locked(std::unique_ptr<Appender> app) :
    app{std::move(app)} {
}

void Locked::log(Logger::Level l, const std::string& msg) {
    std::scoped_lock lck{mtx};
    app->log(l, msg);
}

void Locked::flush() {
    std::scoped_lock lck{mtx};
    app->flush();
}

} // namespace sl::appender
