#include <sl/transformers.hpp>

#include <algorithm>
#include <cctype>
#include <locale>

namespace sl::transformer {

std::string Lowercase::transform(std::string msg) {
    std::transform(msg.begin(), msg.end(), msg.begin(),
                   [](auto c) { return std::tolower(c); });
    return msg;
}

std::string NoTrailingBlank::transform(std::string msg) {
    msg.erase(std::find_if(msg.rbegin(), msg.rend(),
                           [](auto c) { return !std::isspace(c) && c != '\n'; })
                  .base(),
              msg.end());

    return msg;
}

} // namespace sl::transformer
