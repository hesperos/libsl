#include <sl/scoped_log.hpp>

namespace sl {
ScopedLog::ScopedLog(sl::Logger& logger, sl::Logger::Level level,
                     std::source_location sl) :
    logger{logger},
    level{level},
    sl{sl} {
    logger.log(level, "--> scope enter", sl);
}

ScopedLog::~ScopedLog() {
    logger.log(level, "<-- scope exit", sl);
}
} // namespace sl
