#include <sl/appenders.hpp>
#include <sl/factory.hpp>
#include <sl/transformers.hpp>

#include <iostream>

namespace {

class FakeRaiiWrapper : public sl::Appender {
public:
    FakeRaiiWrapper(std::shared_ptr<sl::Appender> app) :
        app{app} {
    }

    void log(sl::Logger::Level l, const std::string& msg) override {
        app->log(l, msg);
    }

    void flush() override {
        app->flush();
    }

private:
    std::shared_ptr<sl::Appender> app;
};

auto sharedCout = std::make_shared<sl::appender::Locked>(
    std::make_unique<sl::appender::Ostream>(std::cout,
                                            sl::Logger::Level::lowest));

auto sharedCerr = std::make_shared<sl::appender::Locked>(
    std::make_unique<sl::appender::Ostream>(std::cerr,
                                            sl::Logger::Level::Warning));
} // namespace

namespace sl {

Logger makeStreamsLogger(std::string prefix) {
    sl::Logger l{prefix};
    l.addAppender(std::make_unique<FakeRaiiWrapper>(sharedCout));
    l.addAppender(std::make_unique<FakeRaiiWrapper>(sharedCerr));
    l.addTransformer(std::make_unique<transformer::NoTrailingBlank>());
    l.addTransformer(std::make_unique<transformer::Lowercase>());
    return l;
}

} // namespace sl
